<?php

use App\Core\Router;

require_once dirname(__DIR__).'/vendor/autoload.php';

(new Router())->dispatchRequest();
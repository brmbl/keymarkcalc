<?php

namespace App\Test\Model;

use App\Core\Exception\ExpressionException;
use App\Core\ValueObject\Digit;
use App\Core\ValueObject\Expression;
use App\Core\ValueObject\PseudoExpression;
use App\Model\ExpressionModifier;
use PHPUnit\Framework\TestCase;

class ExpressionModifierTest extends TestCase
{
    /** @var ExpressionModifier */
    private $expressionModifier;

    public function setUp(): void
    {
        parent::setUp();

        $this->expressionModifier = new ExpressionModifier();
    }

    /**
     * @param PseudoExpression $pseudoExpression
     * @param Digit            $digit
     * @param Expression       $expectedExpression
     *
     * @dataProvider modifyDataProvider
     * @throws ExpressionException
     */
    public function testItModify(PseudoExpression $pseudoExpression, Digit $digit, Expression $expectedExpression)
    {
        //when
        $expression = $this->expressionModifier->modify($pseudoExpression, $digit);

        //then
        $this->assertInstanceOf(Expression::class, $expression);
        $this->assertEquals((string) $expectedExpression, (string) $expression);
    }

    public function modifyDataProvider(): array
    {
        return [
            [PseudoExpression::fromString('2+' . PseudoExpression::MARK), Digit::fromValue(4), Expression::fromString('2+4')],
            [PseudoExpression::fromString('2+(8-' . PseudoExpression::MARK . ')'), Digit::fromValue(4), Expression::fromString('2+(8-4)')],
            [PseudoExpression::fromString('2+(' . PseudoExpression::MARK . ')'), Digit::fromValue(4), Expression::fromString('2+4')],
        ];
    }
}
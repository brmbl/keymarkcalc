<?php

namespace App\Test\Model;

use App\Core\Exception\ExpressionException;
use App\Core\ValueObject\Digit;
use App\Core\ValueObject\Expression;
use App\Core\ValueObject\ExtractedExpression;
use App\Core\ValueObject\MathAction;
use App\Core\ValueObject\MathOperator;
use App\Core\ValueObject\PseudoExpression;
use App\Model\ExpressionExtractor;
use PHPUnit\Framework\TestCase;

class ExpressionExtractorTest extends TestCase
{
    /** @var ExpressionExtractor */
    private $expressionExtractor;

    public function setUp(): void
    {
        parent::setUp();

        $this->expressionExtractor = new ExpressionExtractor();
    }

    /**
     * @param Expression          $expression
     * @param ExtractedExpression $expectedExtractedException
     *
     * @dataProvider extractorDataProvider
     * @throws ExpressionException
     */
    public function testItExtract(Expression $expression, ExtractedExpression $expectedExtractedException)
    {
        //when
        $extractedExpression = $this->expressionExtractor->extract($expression);

        //then
        $this->assertInstanceOf(ExtractedExpression::class, $extractedExpression);
        $this->assertEquals(
            (string)$expectedExtractedException->getMathAction(),
            (string)$extractedExpression->getMathAction()
        );
        $this->assertEquals(
            (string)$expectedExtractedException->getPseudoExpression(),
            (string)$extractedExpression->getPseudoExpression()
        );
    }

    public function extractorDataProvider(): array
    {
        $expressionA = Expression::fromString('10-(2+2*2+(1+1/(3-2*1)))');
        $extractedExpressionA = new ExtractedExpression(
            new MathAction(
                Digit::fromValue(-2),
                Digit::fromValue(1),
                MathOperator::fromString(MathOperator::MULTIPLY)
            ),
            PseudoExpression::fromString('10-(2+2*2+(1+1/(3' . PseudoExpression::MARK . ')))')
        );
        $expressionB = Expression::fromString('10-(2+2)');
        $extractedExpressionB = new ExtractedExpression(
            new MathAction(
                Digit::fromValue(2),
                Digit::fromValue(2),
                MathOperator::fromString(MathOperator::PLUS)
            ),
            PseudoExpression::fromString('10-(' . PseudoExpression::MARK . ')')
        );
        $expressionC = Expression::fromString('10-(2+2*2)');
        $extractedExpressionC = new ExtractedExpression(
            new MathAction(
                Digit::fromValue(2),
                Digit::fromValue(2),
                MathOperator::fromString(MathOperator::MULTIPLY)
            ),
            PseudoExpression::fromString('10-(2+' . PseudoExpression::MARK . ')')
        );
        $expressionD = Expression::fromString('-10*-5');
        $extractedExpressionD = new ExtractedExpression(
            new MathAction(
                Digit::fromValue(-10),
                Digit::fromValue(-5),
                MathOperator::fromString(MathOperator::MULTIPLY)
            ),
            PseudoExpression::fromString(PseudoExpression::MARK)
        );
        return [
            [$expressionA, $extractedExpressionA],
            [$expressionB, $extractedExpressionB],
            [$expressionC, $extractedExpressionC],
            [$expressionD, $extractedExpressionD],
        ];
    }
}
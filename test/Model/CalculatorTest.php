<?php

namespace App\Test\Model;

use App\Core\Exception\MathException;
use App\Core\ValueObject\Digit;
use App\Model\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /** @var Calculator */
    private $calculator;

    public function setUp(): void
    {
        parent::setUp();

        $this->calculator = new Calculator();
    }

    /**
     * @param Digit $a
     * @param Digit $b
     * @param Digit $expectedSum
     *
     * @dataProvider plusDataProvider
     * @throws \Exception
     */
    public function testPlus(Digit $a, Digit $b, Digit $expectedSum)
    {
        //when
        $sum = $this->calculator->plus($a, $b);

        //then
        $this->assertEquals((string) $expectedSum, (string) $sum);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function plusDataProvider(): array
    {
        return [
            [Digit::fromValue(1), Digit::fromValue(2), Digit::fromValue(3)],
            [Digit::fromValue(-1), Digit::fromValue(2), Digit::fromValue(1)],
            [Digit::fromValue(-5), Digit::fromValue(2), Digit::fromValue(-3)]
        ];
    }

    /**
     * @param Digit $a
     * @param Digit $b
     * @param Digit $expectedSum
     *
     * @dataProvider minusDataProvider
     * @throws \Exception
     */
    public function testMinus(Digit $a, Digit $b, Digit $expectedSum)
    {
        //when
        $sum = $this->calculator->minus($a, $b);

        //then
        $this->assertEquals((string)$expectedSum, (string) $sum);
    }

    public function minusDataProvider(): array
    {
        return [
            [Digit::fromValue(2), Digit::fromValue(1), Digit::fromValue(1)],
            [Digit::fromValue(-2), Digit::fromValue(1), Digit::fromValue(-3)],
            [Digit::fromValue(-2), Digit::fromValue(-4), Digit::fromValue(2)],
        ];
    }

    /**
     * @param Digit $a
     * @param Digit $b
     * @param Digit $expectedSum
     *
     * @throws \Exception
     * @dataProvider multiplyDataProvider
     */
    public function testMultiply(Digit $a, Digit $b, Digit $expectedSum)
    {
        //when
        $sum = $this->calculator->multiply($a, $b);

        //then
        $this->assertEquals((string) $expectedSum, (string) $sum);
    }

    public function multiplyDataProvider(): array 
    {
        return [
            [Digit::fromValue(1), Digit::fromValue(1), Digit::fromValue(1)],
            [Digit::fromValue(2), Digit::fromValue(2), Digit::fromValue(4)],
            [Digit::fromValue(-2), Digit::fromValue(2), Digit::fromValue(-4)],
            [Digit::fromValue(-5), Digit::fromValue(-5), Digit::fromValue(25)],
            [Digit::fromValue(5), Digit::fromValue(-2), Digit::fromValue(-10)],
        ];
    }

    /**
     * @param Digit $a
     * @param Digit $b
     * @param Digit $expectedSum
     *
     * @throws \Exception
     * @dataProvider divideDataProvider
     */
    public function testDivide(Digit $a, Digit $b, Digit $expectedSum)
    {
        //when
        $sum = $this->calculator->divide($a, $b);

        //then
        $this->assertEquals($expectedSum, $sum);
    }

    public function divideDataProvider()
    {
        return [
            [Digit::fromValue(6), Digit::fromValue(2), Digit::fromValue(3)],
            [Digit::fromValue(7), Digit::fromValue(2), Digit::fromValue(3.5)],
            [Digit::fromValue(1), Digit::fromValue(1), Digit::fromValue(1)],
            [Digit::fromValue(0), Digit::fromValue(6), Digit::fromValue(0)],
        ];
    }

    public function testItHandleDivideByZero()
    {
        $this->expectException(MathException::class);
        $this->expectExceptionMessage(MathException::DIVISION_BY_ZERO);

        //when
        $this->calculator->divide(Digit::fromValue(5), Digit::fromValue(0));

    }
}
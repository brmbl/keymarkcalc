<?php

namespace App\Test\Core\ValueObject;

use App\Core\Exception\ExpressionException;
use App\Core\ValueObject\Expression;
use PHPUnit\Framework\TestCase;

class ExpressionTest extends TestCase
{

    /**
     * @param string $expressionString
     *
     * @dataProvider validExpressionDataProvider
     * @throws ExpressionException
     */
    public function testItCreateExpression(string $expressionString): void
    {
        //when
        $expression = Expression::fromString($expressionString);

        //then
        $this->assertInstanceOf(Expression::class, $expression);
        $this->assertEquals($expressionString, (string) $expression);
    }

    public function validExpressionDataProvider()
    {
        return [
            ['2+2'],
            ['7+(2+2*2)*7/(5-3)'],
            ['7+(2.5+2*2)*7/(5-3)'],
            ['-7+(2+2*2)*7/(5-3)'],
            ['-7-2+(2*2)*7/(5-3)'],
            ['-7-2+(2*2/(1*2))*7/(5-3)'],
        ];
    }

    public function testItStripSpace()
    {
        //given
        $string = '7 + ( 2 + 2 * 2 )*7/(5-3)';
        $expectedString = '7+(2+2*2)*7/(5-3)';

        //when
        $expression = Expression::fromString($string);

        //then
        $this->assertInstanceOf(Expression::class, $expression);
        $this->assertEquals($expectedString, (string) $expression);
    }

    /**
     * @param string $expressionString
     * @param string $message
     *
     * @dataProvider invalidExpressionDataProvider
     * @throws ExpressionException
     */
    public function testItThrowExceptionOnInvalid(string $expressionString, string $message)
    {
        //then
        $this->expectException(ExpressionException::class);
        $this->expectExceptionMessage($message);

        //when
        Expression::fromString($expressionString);
    }

    public function invalidExpressionDataProvider(): array
    {
        return [
            ['2@2!a/b1+2', ExpressionException::INVALID_CHARACTERS],
            ['2+(2+8+(2+2)', ExpressionException::INVALID_BRACKETS],
            ['2+2+8+)2+2(', ExpressionException::INVALID_BRACKETS],
            ['2++(2+8)+(2+2)', ExpressionException::INVALID_FORMAT],
            ['+2+(2+8)+(2+2)', ExpressionException::INVALID_FORMAT],
            ['2(2+8)+(2+2)', ExpressionException::INVALID_FORMAT],
            ['2*/(2+8)+(2+2)', ExpressionException::INVALID_FORMAT],
            ['--2*/(2+8)+(2+2)', ExpressionException::INVALID_FORMAT],
            ['*2*/(2+8)+(2+2)', ExpressionException::INVALID_FORMAT],
        ];
    }
}
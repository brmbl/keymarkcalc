<?php

namespace App\Test\Application;

use App\Application\CalculationService;
use App\Core\ValueObject\Digit;
use App\Core\ValueObject\Expression;
use App\Model\Calculator;
use App\Model\ExpressionExtractor;
use App\Model\ExpressionModifier;
use PHPUnit\Framework\TestCase;

class CalculationServiceTest extends TestCase
{
    /** @var ExpressionExtractor */
    private $expressionExtractor;

    /** @var Calculator */
    private $calculator;

    /** @var ExpressionModifier */
    private $expressionModifier;

    /** @var CalculationService */
    private $calculationService;

    public function setUp(): void
    {
        parent::setUp();

        $this->expressionExtractor = new ExpressionExtractor();
        $this->calculator = new Calculator();
        $this->expressionModifier = new ExpressionModifier();
        $this->calculationService = new CalculationService();
    }

    /**
     * @param Expression $expression
     * @param Digit      $expectedResult
     *
     * @throws \Exception
     * @dataProvider expressionDataProvider
     */
    public function testItCalculateExpression(Expression $expression, Digit $expectedResult)
    {
        //when
        $result = $this->calculationService->calculateExpression($expression);

        //then
        $this->assertInstanceOf(Digit::class, $result);
        $this->assertEquals((string) $expectedResult, (string) $result);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function expressionDataProvider(): array
    {
        return [
            [Expression::fromString('2+2*2'), Digit::fromValue(6)],
            [Expression::fromString('2*(10-5)'), Digit::fromValue(10)],
            [Expression::fromString('-2*(-10--5)'), Digit::fromValue(10)],
            [Expression::fromString('-2*-4'), Digit::fromValue(8)],
            [Expression::fromString('2*0.5'), Digit::fromValue(1)],
            [Expression::fromString('2/-0.5'), Digit::fromValue(-4)],
            [Expression::fromString('10+(20/(5-3)*2+(10-20))'), Digit::fromValue(20)],
            [Expression::fromString('-10*-20'), Digit::fromValue(200)],
            [Expression::fromString('-34,1286*20.17826'), Digit::fromString('-688.655764236')],
        ];
    }
}
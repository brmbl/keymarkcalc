# Installation

To install application clone it on your machine.
Run composer install to install PhpUnit.
# Usage
### Console usage
To run console command, run `bin/console` to see command description.
To run application type for example `bin/console calc expression=2+2*2`

### Web usage
To run application throw the browser setup docker container first, run
`docker-compose up -d`, by default it run's on port 80.
To access application go in browser for url http://localhost and put your expression to calculator.
# Test
To run Unit tests type in console `composer run-script test`
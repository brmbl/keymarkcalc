<?php

namespace App\Core\Exception;


class ExpressionException extends \Exception
{
    public const INVALID_CHARACTERS = 'Expression contains invalid characters';

    public const INVALID_BRACKETS = 'Expression contains invalid brackets structure';

    public const INVALID_FORMAT = 'Expression format invalid';
}
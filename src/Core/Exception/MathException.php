<?php

namespace App\Core\Exception;


class MathException extends \Exception
{
    public const DIVISION_BY_ZERO = 'Division by zero';
}
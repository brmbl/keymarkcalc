<?php

namespace App\Core\Factory;


use App\Port\Command\ConsoleCommand;

class CommandFactory
{
    /**
     * @param array $params
     *
     * @return array
     * @throws \Exception
     */
    public static function createCommand(array $params): array
    {
        $requiredCommand = array_shift($params);
        $createdCommand = null;
        $options = [];
        foreach ($params as $paramString) {
            if (!preg_match("/\=/", $paramString)) {
                throw new \Exception('Option should contain equal symbol');
            }
            $option = explode('=', $paramString);
            $options[$option[0]] = $option[1];
        }

        foreach (self::getCommands() as $commandClass) {
            /** @var ConsoleCommand $command */
            $command = new $commandClass;
            $command->configure();
            if ($command->getName() === $requiredCommand) {
                if (array_intersect($command->getRequiredOptions(),
                        array_keys($options)) !== $command->getRequiredOptions()) {
                    throw new \Exception('Not all required options is present');
                }
                $createdCommand = $command;
                break;
            }
        }

        if (is_null($createdCommand)) {
            throw new \Exception("Command name $requiredCommand is undefined");
        }

        return [$createdCommand, $options];
    }

    public static function getCommandsInfo()
    {
        $commandsInfo = [];
        foreach (self::getCommands() as $commandClass) {
            /** @var ConsoleCommand $command */
            $command = new $commandClass;
            $command->configure();

            $commandsInfo[] = $command->renderInfo();
        }

        return $commandsInfo;
    }

    private function getCommands(): array
    {
        return include __DIR__ . '/../../../config/commands.php';
    }
}
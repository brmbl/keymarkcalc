<?php

namespace App\Core\ValueObject;

class PseudoExpression
{
    public const MARK = '@';

    /** @var string */
    private $value;

    /**
     * @param string $value
     *
     * @throws \Exception
     */
    private function __construct(string $value)
    {
        if (!preg_match("/\\" . self::MARK . "/", $value) > 0) {
            throw new \Exception('Pseudo expression must contain at least one "' . self::MARK . '" mark');
        }
        if (substr_count(self::MARK, $value) > 1) {
            throw new \Exception('Pseudo expression must contain only one "' . self::MARK . '" mark');
        }

        $this->value = $value;
    }

    /**
     * @param string $value
     *
     * @return PseudoExpression
     * @throws \Exception
     */
    public static function fromString(string $value): PseudoExpression
    {
        return new self($value);
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
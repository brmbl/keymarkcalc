<?php

namespace App\Core\ValueObject;


class Request
{
    /** @var string */
    private $uri = '';

    /** @var array */
    public $get;

    /** @var array */
    public $post;

    private function __construct()
    {
        $this->uri = trim($_SERVER['REQUEST_URI'], '/');

        foreach ($_GET as $key => $param) {
            $this->get[$key] = trim(htmlentities(strip_tags($param, ENT_QUOTES)));
        }
        foreach ($_POST as $key => $param) {
            $this->post[$key] = trim(htmlentities(strip_tags($param, ENT_QUOTES)));
        }
    }

    /**
     * @return Request
     */
    public static function instance(): Request
    {
        return new self();
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        if (preg_match("/\?/", $this->uri)) {
            $parts = explode('?', $this->uri);
            return $parts[0];
        }

        return $this->uri;
    }
}
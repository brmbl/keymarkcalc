<?php

namespace App\Core\ValueObject;


class Digit
{
    /** @var float|int */
    private $value;

    /**
     * @param int|float $value
     *
     * @throws \Exception
     */
    private function __construct($value)
    {
        if (!is_int($value)) {
            if (!is_float($value)) {
                throw new \Exception("Digit must be int or float type");
            }
        }

        $this->value = $value;
    }


    /**
     * @param int|float $value
     *
     * @return Digit
     * @throws \Exception
     */
    public static function fromValue($value): Digit
    {
        return new self($value);
    }

    /**
     * @param string $value
     *
     * @return Digit
     * @throws \Exception
     */
    public static function fromString(string $value): Digit
    {
        if (preg_match("/^-?\d+$/", $value)) {
            return new self(intval($value));
        }

        if (preg_match("/^-?\d+[\.\,]\d+$/", $value)) {
            $value = preg_replace("/,/", ".", $value);
            return new self(floatval($value));
        }

        return new self($value);
    }

    public function getValue()
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }
}
<?php

namespace App\Core\ValueObject;

use App\Core\Exception\ExpressionException;

class Expression
{
    /** @var string */
    private $value;

    /**
     * @param string $value
     *
     * @throws ExpressionException
     */
    private function __construct(string $value)
    {
        $stripedValue = str_replace(' ', '', $value);
        if (preg_match("/[^\d\+\-\*\/\(\)\.\,]/", $stripedValue)) {
            throw new ExpressionException(ExpressionException::INVALID_CHARACTERS);
        }
        if (!$this->isBracketsCorrect($stripedValue)) {
            throw new ExpressionException(ExpressionException::INVALID_BRACKETS);
        }
        if (!$this->isValidExpressionFormat($stripedValue)) {
            throw new ExpressionException(ExpressionException::INVALID_FORMAT);
        }

        $this->value = $stripedValue;
    }

    /**
     * @param string $string
     *
     * @return Expression
     * @throws ExpressionException
     */
    public static function fromString(string $string): Expression
    {
        return new self($string);
    }

    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @param string $expression
     *
     * @return bool
     */
    private function isValidExpressionFormat(string $expression): bool
    {
        if (!preg_match("/^-?\d+([\.\,]\d+)?[\+\-\*\/][\(]?-?\d+([\.\,]\d+)?/", $expression)) {
            return false;
        }

        $expressionParts = str_split($expression);

        foreach ($expressionParts as $k => $v) {
            if (MathOperator::isOperator($v)) {
                if ($k === 0) {
                    if ($v === MathOperator::MINUS){
                        continue;
                    }

                    return false;
                }
                if (MathOperator::isOperator($expressionParts[$k - 1])) {
                    if ($v === MathOperator::MINUS) {
                        if (MathOperator::isOperator($expressionParts[$k - 2])) {
                            return false;
                        }

                        continue;
                    }
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param string $expression
     *
     * @return bool
     */
    private function isBracketsCorrect(string $expression): bool
    {
        if (substr_count($expression, '(') !== substr_count($expression, ')')){
            return false;
        }

        $expressionParts = str_split($expression);
        $bracketState = 0;

        foreach ($expressionParts as $k => $v) {
            if ($v === '(') {
                $bracketState++;
            }
            if ($v === ')') {
                $bracketState--;
            }

            if ($bracketState < 0) {
                return false;
            }
        }

        if ($bracketState !== 0) {
            return false;
        }

        return true;
    }
}
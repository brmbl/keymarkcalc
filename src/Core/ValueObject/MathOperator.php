<?php

namespace App\Core\ValueObject;

class MathOperator
{
    public const PLUS = '+';

    public const MINUS = '-';

    public const MULTIPLY = '*';

    public const DIVIDE = '/';

    /** @var string */
    private $value;

    /**
     * @param string $value
     *
     * @throws \Exception
     */
    private function __construct(string $value)
    {
        if (!self::isOperator($value)) {
            throw new \Exception('Undefined math operator');
        }

        $this->value = $value;
    }

    /**
     * @param string $string
     *
     * @return MathOperator
     * @throws \Exception
     */
    public static function fromString(string $string): MathOperator
    {
        return new self($string);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    public static function isOperator(string $value): bool
    {
        if (in_array($value, [
            self::PLUS,
            self::MINUS,
            self::MULTIPLY,
            self::DIVIDE,
        ])) {
            return true;
        }

        return false;
    }
}
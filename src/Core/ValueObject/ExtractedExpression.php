<?php

namespace App\Core\ValueObject;

class ExtractedExpression
{
    /** @var MathAction */
    private $mathAction;

    /** @var PseudoExpression */
    private $pseudoExpression;

    /**
     * @param MathAction       $mathAction
     * @param PseudoExpression $pseudoExpression
     */
    public function __construct(MathAction $mathAction, PseudoExpression $pseudoExpression)
    {
        $this->mathAction = $mathAction;
        $this->pseudoExpression = $pseudoExpression;
    }

    /**
     * @return MathAction
     */
    public function getMathAction(): MathAction
    {
        return $this->mathAction;
    }

    /**
     * @return PseudoExpression
     */
    public function getPseudoExpression(): PseudoExpression
    {
        return $this->pseudoExpression;
    }
}
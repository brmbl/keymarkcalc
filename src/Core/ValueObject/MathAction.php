<?php

namespace App\Core\ValueObject;

class MathAction
{
    /** @var Digit */
    private $a;
    
    /** @var Digit */
    private $b;
    
    /** @var MathOperator */
    private $operator;

    /**
     * @param Digit        $a
     * @param Digit        $b
     * @param MathOperator $operator
     */
    public function __construct(Digit $a, Digit $b, MathOperator $operator)
    {
        $this->a = $a;
        $this->b = $b;
        $this->operator = $operator;
    }

    public function getA(): Digit
    {
        return $this->a;
    }

    public function getB(): Digit
    {
        return $this->b;
    }

    public function getOperator(): string
    {
        return (string) $this->operator;
    }

    public function __toString(): string
    {
        return $this->getA() . $this->getOperator() . $this->getB();
    }
}
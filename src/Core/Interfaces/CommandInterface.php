<?php

namespace App\Core\Interfaces;


interface CommandInterface
{
    public function configure(): void;

    public function execute(array $options): void;
}
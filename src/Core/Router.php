<?php

namespace App\Core;


use App\Core\ValueObject\Request;
use App\Port\Controller\CalcController;

class Router
{
    /** @var array */
    private $routes = [];

    /** @var bool */
    private $matchRoute = false;

    public function __construct()
    {
        $this->routes = include dirname(__DIR__) . '/../config/routes.php';
    }

    public function dispatchRequest(): void
    {
        $request = Request::instance();
        if ($request->getUri() === '') {
            $controller = new CalcController();
            $controller->show($request);
            $this->matchRoute = true;
        } else {
            foreach ($this->routes as $uri => $address) {
               if ($request->getUri() === $uri) {
                   $this->matchRoute = true;
                   $addressParts = explode('@', $address);
                   $className = 'App\Port\Controller\\' . $addressParts[0];
                   call_user_func_array([new $className, $addressParts[1]], [$request]);
               }
            }
        }

        if (!$this->matchRoute) {
            self::to404();
        }
    }

    public static function to404(): void
    {
        header('HTTP/1.1 404 Not Found');
        $_GET['e'] = 404;
        include dirname(__DIR__) . '/View/404.php';
        exit;
    }
}
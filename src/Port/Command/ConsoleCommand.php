<?php

namespace App\Port\Command;

use App\Core\Interfaces\CommandInterface;

abstract class ConsoleCommand implements CommandInterface
{
    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var array */
    private $options = [];

    /** @var string */
    private $example;

    abstract public function configure(): void;

    abstract public function execute(array $options): void;

    /**
     * @param string $example
     *
     * @return ConsoleCommand
     */
    public function setExample(string $example): ConsoleCommand
    {
        $this->example = $example;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    protected function setName(string $name): ConsoleCommand
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return ConsoleCommand
     */
    public function setDescription(string $description): ConsoleCommand
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param string $optionName
     * @param bool   $required
     *
     * @return $this
     */
    protected function addOption(string $optionName, bool $required = false): ConsoleCommand
    {
        $this->options[] = [$optionName, $required];

        return $this;
    }

    /**
     * @return array
     */
    public function getRequiredOptions(): array
    {
        $requiredOptions = [];
        foreach ($this->options as $option) {
            if ($option[1]) {
                $requiredOptions[] = $option[0];
            }
        }

        return $requiredOptions;
    }

    /**
     * @return array
     */
    public function renderInfo(): array
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'options' => $this->options,
            'example' => $this->example,
        ];
    }
}
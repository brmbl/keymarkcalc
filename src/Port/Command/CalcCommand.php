<?php

namespace App\Port\Command;


use App\Application\CalculationService;
use App\Core\ValueObject\Expression;

class CalcCommand extends ConsoleCommand
{
    private $calculationService;

    public function __construct()
    {
        $this->calculationService = new CalculationService();
    }

    public function configure(): void
    {
        $this->setName('calc')
            ->setDescription('calculate mathematical expression')
            ->addOption('expression', true)
            ->setExample('bin/console calc expression=2+2*2*(6-3)');
    }

    public function execute(array $options): void
    {
        try {
            $expression = Expression::fromString($options['expression']);
            $result = $this->calculationService->calculateExpression($expression);
            echo "Expression $expression result is " . $result . "\n";
        } catch (\Exception $e) {
            echo "Error: " . $e->getMessage() . "\n";
        }
    }
}
<?php

namespace App\Port\Controller;


use App\Application\CalculationService;
use App\Core\ValueObject\Expression;
use App\Core\ValueObject\Request;

class CalcController
{
    public function show()
    {
        include dirname(__DIR__) . '/../View/home.php';
    }

    public function calculate(Request $request)
    {
        try {
            $expression = Expression::fromString($request->get['expression']);
            $calculationService = new CalculationService();
            $result = $calculationService->calculateExpression($expression);

            header('HTTP/1.1 200 OK');

            echo (string) $result;
        } catch (\Exception $e) {
            header('HTTP/1.1 400 Bad Request');
            echo $e->getMessage();
        }
    }
}
<?php

namespace App\Model;


use App\Core\Exception\ExpressionException;
use App\Core\ValueObject\Digit;
use App\Core\ValueObject\Expression;
use App\Core\ValueObject\ExtractedExpression;
use App\Core\ValueObject\MathAction;
use App\Core\ValueObject\MathOperator;
use App\Core\ValueObject\PseudoExpression;

class ExpressionExtractor
{
    public const ALL_IN_BRACKETS_PATTERN = "/\(.*?\).*/";

    public const MULTIPLY_DIVIDE_PATTERN = "/(\-?\d+([\,\.]?\d+)?)([\*\/])(\-?\d+([\,\.]?\d+)?)/";

    public const PLUS_MINUS_PATTERN = "/(\-?\d+([\,\.]?\d+)?)([\+\-])(-?\d+([\,\.]?\d+)?)/";

    /**
     * @param Expression            $expression
     * @param PseudoExpression|null $previousExpression
     *
     * @return ExtractedExpression|null
     * @throws ExpressionException
     */
    public function extract(Expression $expression, PseudoExpression $previousExpression = null): ?ExtractedExpression
    {
        preg_match(self::ALL_IN_BRACKETS_PATTERN, (string)$expression, $matchBrackets);
        if (!empty($matchBrackets)) {

            list($inBrackets, $mark) = $this->extractInBrackets($expression);
            if ($previousExpression) {
                $previousExpression = PseudoExpression::fromString(str_replace(PseudoExpression::MARK, $mark,
                    $previousExpression));
            } else {
                $previousExpression = PseudoExpression::fromString($mark);
            }
            $internalExpression = Expression::fromString($inBrackets);
            return $this->extract($internalExpression, $previousExpression);
        }

        preg_match(self::MULTIPLY_DIVIDE_PATTERN, (string)$expression, $matchMuDi);
        if (!empty($matchMuDi)) {
            $mark = preg_replace(self::MULTIPLY_DIVIDE_PATTERN, PseudoExpression::MARK, (string)$expression);
            if ($previousExpression) {
                $previousExpression = PseudoExpression::fromString(str_replace(PseudoExpression::MARK, $mark,
                    $previousExpression));
            } else {
                $previousExpression = PseudoExpression::fromString($mark);
            }
            $mathAction = new MathAction(
                Digit::fromString($matchMuDi[1]),
                Digit::fromString($matchMuDi[4]),
                MathOperator::fromString($matchMuDi[3])
            );

            return new ExtractedExpression($mathAction, $previousExpression);
        }

        preg_match(self::PLUS_MINUS_PATTERN, (string)$expression, $matchPlusMinus);
        if (!empty($matchPlusMinus)) {
            $mark = preg_replace(self::PLUS_MINUS_PATTERN, PseudoExpression::MARK, (string)$expression);
            if ($previousExpression) {
                $previousExpression = PseudoExpression::fromString(str_replace(PseudoExpression::MARK, $mark,
                    $previousExpression));
            } else {
                $previousExpression = PseudoExpression::fromString($mark);
            }
            $mathAction = new MathAction(
                Digit::fromValue(empty($matchPlusMinus[2]) ? intval($matchPlusMinus[1]) : floatval($matchPlusMinus[1])),
                Digit::fromValue(empty($matchPlusMinus[5]) ? intval($matchPlusMinus[4]) : floatval($matchPlusMinus[4])),
                MathOperator::fromString($matchPlusMinus[3])
            );

            return new ExtractedExpression($mathAction, $previousExpression);
        }

        return null;
    }

    /**
     * @param Expression $expression
     *
     * @return array
     * @throws ExpressionException
     */
    private function extractInBrackets(Expression $expression): array
    {
        $bracketWasOpened = false;
        $bracketState = 0;
        $expressionParts = str_split((string)$expression);
        $inBrackets = '';
        foreach ($expressionParts as $k => $part) {
            if ($part === '(') {
                if ($bracketWasOpened) {
                    $bracketState++;
                    $inBrackets .= $part;
                    $expressionParts[$k] = '~';
                } else {
                    $bracketWasOpened = true;
                    $bracketState++;
                }
            } elseif ($part === ')') {
                if (!$bracketWasOpened) {
                    throw new ExpressionException(ExpressionException::INVALID_BRACKETS);
                } else {
                    $bracketState--;
                    if ($bracketState === 0) {
                        break;
                    }
                    $inBrackets .= $part;
                    $expressionParts[$k] = '~';
                }
            } else {
                if ($bracketWasOpened) {
                    $inBrackets .= $part;
                    $expressionParts[$k] = '~';
                }
            }
        }
        $expression = implode('', $expressionParts);
        $mark = preg_replace("/\~+/", PseudoExpression::MARK, $expression);

        return [$inBrackets, $mark];
    }
}
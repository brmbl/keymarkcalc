<?php

namespace App\Model;

use App\Core\Exception\ExpressionException;
use App\Core\ValueObject\Digit;
use App\Core\ValueObject\Expression;
use App\Core\ValueObject\PseudoExpression;

class ExpressionModifier
{
    public const ONLY_DIGIT_IN_BRACKETS_PATTERN = "/(\()(-?\d+([\.\,]\d+)?)(\))/";

    /**
     * @param PseudoExpression $pseudoExpression
     * @param Digit            $digit
     *
     * @return Digit | Expression
     * @throws ExpressionException
     */
    public function modify(PseudoExpression $pseudoExpression, Digit $digit)
    {
        $modified = str_replace(PseudoExpression::MARK, (string) $digit, (string) $pseudoExpression);
        preg_match(self::ONLY_DIGIT_IN_BRACKETS_PATTERN, $modified, $matches);
        if (!empty($matches)){
            $modified = preg_replace(self::ONLY_DIGIT_IN_BRACKETS_PATTERN, $matches[2], $modified);
        }
        try {
            return Digit::fromString($modified);
        } catch (\Exception $e) {
            return Expression::fromString($modified);
        }
    }
}
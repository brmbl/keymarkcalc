<?php

namespace App\Model;

use App\Core\Exception\MathException;
use App\Core\ValueObject\Digit;

class Calculator
{
    /**
     * @param Digit $a
     * @param Digit $b
     *
     * @return Digit
     * @throws \Exception
     */
    public function plus(Digit $a, Digit $b): Digit
    {
        return Digit::fromValue((string) $a + (string) $b);
    }

    /**
     * @param Digit $a
     * @param Digit $b
     *
     * @return Digit
     * @throws \Exception
     */
    public function minus(Digit $a, Digit $b): Digit
    {
        return Digit::fromValue((string) $a - (string) $b);
    }

    /**
     * @param $a
     * @param $b
     *
     * @return Digit
     * @throws \Exception
     */
    public function multiply($a, $b): Digit
    {
        return Digit::fromValue((string) $a * (string) $b);
    }

    /**
     * @param Digit $a
     * @param Digit $b
     *
     * @return Digit
     * @throws \Exception
     */
    public function divide(Digit $a, Digit $b): Digit
    {
        if ((string) $b === "0" ) {
            throw new MathException(MathException::DIVISION_BY_ZERO);
        }

        return Digit::fromValue((string) $a / (string) $b);
    }
}
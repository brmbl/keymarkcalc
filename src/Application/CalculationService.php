<?php

namespace App\Application;

use App\Core\ValueObject\Digit;
use App\Core\ValueObject\Expression;
use App\Core\ValueObject\MathAction;
use App\Core\ValueObject\MathOperator;
use App\Model\Calculator;
use App\Model\ExpressionExtractor;
use App\Model\ExpressionModifier;

class CalculationService
{
    /** @var ExpressionExtractor */
    private $expressionExtractor;

    /** @var Calculator */
    private $calculator;

    /** @var ExpressionModifier */
    private $expressionModifier;

    public function __construct()
    {
        $this->expressionExtractor = new ExpressionExtractor();
        $this->calculator = new Calculator();
        $this->expressionModifier = new ExpressionModifier();
    }

    /**
     * @param Expression $expression
     *
     * @return Digit
     * @throws \Exception
     */
    public function calculateExpression(Expression $expression): Digit
    {
        $extractedExpression = $this->expressionExtractor->extract($expression);
        $result = $this->calculateMathAction($extractedExpression->getMathAction());
        $newExpression = $this->expressionModifier->modify($extractedExpression->getPseudoExpression(), $result);

        if ($newExpression instanceof Expression) {
            return $this->calculateExpression($newExpression);
        }

        return $newExpression;
    }

    /**
     * @param MathAction $mathAction
     *
     * @return Digit
     * @throws \Exception
     */
    private function calculateMathAction(MathAction $mathAction): Digit
    {
        switch ($mathAction->getOperator()) {
            case MathOperator::PLUS:
                $result = $this->calculator->plus($mathAction->getA(), $mathAction->getB());
                break;
            case MathOperator::MINUS:
                $result = $this->calculator->minus($mathAction->getA(), $mathAction->getB());
                break;
            case MathOperator::MULTIPLY:
                $result = $this->calculator->multiply($mathAction->getA(), $mathAction->getB());
                break;
            case MathOperator::DIVIDE:
                $result = $this->calculator->divide($mathAction->getA(), $mathAction->getB());
                break;
            default:
                throw new \Exception('Undefined math operator');
        }

        return $result;
    }
}